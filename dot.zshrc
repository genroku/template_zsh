#
# .zshrc for linux/macosx
# since 2014/05/24
# last modified 2016/04/17
#

#for debugging
test "$SSH_TTY" != "" && echo "loading .zshrc"


#==================================================
# Set prompt and other shell variables
#==================================================
#PROMPT="%{${fg[red]}%}%B%m[%3d]%b
#$%{${reset_color}%} "
PROMPT="%U%B%F{cyan}%n%f[%m:%3d]%b%u
$ "

RPROMPT=""

#==================================================
# Optiions for zsh
#==================================================

# Set/unset  shell options
setopt alwayslastprompt autocd autolist automenu autoparamkeys
setopt autopushd autoresume autoremoveslash
setopt cdablevars
setopt extendedglob globdots histignoredups ignoreeof longlistjobs
setopt mailwarning noclobber nolistbeep notify
setopt pushdminus pushdsilent pushdtohome pushdignoredups
setopt rcquotes recexact

unsetopt autoparamslash bgnice correct correctall
unsetopt noclobber

#端末ロックを無効にする（Ctrl-Sに割り当てられている）
stty stop undef


#==================================================
# Key bindings
#==================================================
#bindkey '^X^Z' universal-argument ' ' magic-space
#bindkey '^X^A' vi-find-prev-char-skip
#bindkey '^Z' accept-and-hold
#bindkey -s '\M-/' \\\\
#bindkey -s '\M-=' \|

bindkey -e # emacs key bindings
bindkey "^N" down-line-or-search
bindkey "^P" up-line-or-search
bindkey "\M-s" spell-word

#bindkey ' ' magic-space  # also do history expansino on space


#==================================================
# Completions
#==================================================
autoload -U compinit && compinit

function ssh_func_known_hosts (){ 
    if [ -f $HOME/.ssh/known_hosts ]; then
        cat $HOME/.ssh/known_hosts | sed '/^ *$/d' | tr ',' ' ' | cut -d' ' -f1 
    fi
}
_cache_hosts=($(ssh_func_known_hosts))


#==================================================
# Commnand Definitions
#==================================================
unalias -m \*

# Some Checkings
# null

# For general shell commands
alias   cp='nocorrect cp -i'
alias   mv='nocorrect mv -i'
alias   rm='nocorrect rm -i'

alias   ls="nocorrect ls -F"

alias   gd='dirs -v ; echo -n "select number: "; read newdir; cd -"$newdir"'
alias   ppd='popd'
alias   psd='pushd'

alias   rmtilde='find . -name "*~" -exec /bin/rm {} \;'
alias   x='echo "see you!"; exit'

alias   df='df -k'
alias   du='du -k'

alias   gip='curl inet-ip.info'


#==================================================
# Application specific settings
#==================================================
#-------------------------
# for git
#-------------------------
function cd(){
  builtin cd $1
  msg=$(is_git_dir)
  if [ "$msg" = "" ]; then
      RPROMPT=""
  else
      RPROMPT="<$msg>"
  fi

  # clean up
  unset msg
}

function is_git_dir(){
  alias dir_split="echo $(pwd) | sed -e 's;^/;;' -e 's;/; ;g'"
  dir_elmnt=($(dir_split))
  depth=${#dir_elmnt[*]}

  count=0; ckdir=.; rtn=""

  while [ $count -le $depth ]; do
    #echo "$count : $(cd $ckdir;pwd)" 1>&2 #for debugging.
    if [ -e "$ckdir/.git" ]; then
        rtn="git"; break
    fi
    count=$(($count + 1))
    ckdir=$ckdir/..
  done

  echo $rtn

  # clean up
  unalias dir_split; unset dir_elmnt depth count ckdir rtn
}

#-------------------------
# for screen
#-------------------------
if [ $TERM = "screen" ]; then
    function screen_func_pwd()
    {
        echo -ne "\ek$(hostname | cut -d'.' -f1):$(pwd)\e\\"
    }

    function chpwd()
    {
        screen_func_pwd
    }
fi

#-------------------------
# for vim
#-------------------------
type vim > /dev/null && alias vi='vim'


#==================================================
# Other local settings
#==================================================
if [ -f $HOME/.zshrc_local ]; then
    source $HOME/.zshrc_local
fi
